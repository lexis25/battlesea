import java.util.HashMap;
import java.util.Map;

public class PointShip {
    public Map<Character, Integer> latitude = new HashMap<>();

    public static final char[] KEY_ALPHABET = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};
    private char x;
    private int y;

    public PointShip(char latitude, int number) {
        this.x = latitude;
        this.y = number;
        initializeLatitude();

    }

    public PointShip(){
        initializeLatitude();
    }

    private void initializeLatitude() {

        for (int i = 0; i < this.KEY_ALPHABET.length; i++) {
            this.latitude.put(this.KEY_ALPHABET[i], i);
        }
    }

    public PointShip get() {
        int n = latitude.get(x);
        PointShip point = new PointShip((char) n, this.y);
        return point;
    }

    public int getY(){
        return y;
    }

    public int getX(){
        return latitude.get(x);
    }
}
