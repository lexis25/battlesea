import java.awt.*;
import java.util.ArrayList;
import java.awt.Point;

public class Ship {
    private ArrayList<Point> cell = new ArrayList<>();
    private ArrayList<Point> deathCell = new ArrayList<>();

    public static final String BATTLESHIP = "BATTLESHIP";
    public static final String CRUISER = "CRUISER";
    public static final String DESTROYER = "DESTROYER";
    public static final String BOAT = "BOAT";

    private String typeName;
    private int length;

    Ship(String typeName) {
        this.typeName = typeName;
        switch (this.typeName){
            case BATTLESHIP:
                this.length = 4;
                break;
            case CRUISER:
                this.length = 3;
                break;
            case DESTROYER:
                this.length = 2;
                break;
            case BOAT:
                this.length = 1;
                break;
        }
    }

    public String getTypeName() {
        return typeName;
    }

    public void setCell(Point point) {
        cell.add(point);
    }

    public ArrayList<Point> getCell() {
        return cell;
    }

    public ArrayList<Point> getDeathCell(){
        return deathCell;
    }

    public int getLength(){
        return length;
    }

    public void removeCell(int cell) {
        this.deathCell.add(this.cell.get(cell));
        this.cell.remove(cell);

    }
}
