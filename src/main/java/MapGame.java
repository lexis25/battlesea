public class MapGame {
    private final String mapName;
    public static final String MAP_PLAYER = "<< MAP PLAYER >>";
    public static final String MAP_ENEMY = "<< MAP ENEMY >>";

    public static final String WAVE = "^";
    public static final String MISSED = "O";
    public static final String WOUNDED = "X";
    public static final String KILLED = "#";
    public static final String SHIP = "+";

    private String body[][];


    public MapGame(String mapName){
        this.mapName = mapName;
        body = new String[10][10];
        initializeMapGame();
    }

    public void setMap(int x, int y, String title){
        body[x][y] = title;
    }

    public String [][] getMap(){
        return body;
    }

    public String getCell(int x, int y){
        return body[x][y];
    }

    private void initializeMapGame(){
        for (int i = 0; i < this.body.length; i++) {
            for (int j = 0; j < this.body[i].length; j++) {
                this.body[i][j] = WAVE;
            }
        }
    }

    public void render(){
        System.out.println(this.mapName);
        for (int j = 0; j <= 10; j++) {
            if (j == 0) {
                System.out.print("  ");
            } else {
                System.out.print(j + " ");
            }
        }
        System.out.println();
        for (int i = 0; i < this.body.length; i++) {

            System.out.print(PointShip.KEY_ALPHABET[i] + " ");
            for (int j = 0; j < this.body[i].length; j++) {

                System.out.print(this.body[i][j] + " ");
            }
            System.out.println();
        }
    }

}
