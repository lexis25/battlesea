public class Main {

    public static void main(String[] args) {
        BattleSea game = new BattleSea();

        while(true){
            game.start(game.getMapPlayer(),false, MapGame.MAP_PLAYER);

            if(!game.isGame()){
                break;
            }

            game.start(game.getMapEnemyShip(),true, MapGame.MAP_ENEMY);

            if(!game.isGame()){
                break;
            }
        }
    }
}
