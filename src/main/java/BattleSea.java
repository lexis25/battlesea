import java.awt.*;
import java.util.*;
import java.awt.Point;

public class BattleSea {



    private MapGame mapPlayer;
    private MapGame mapEnemyShip;
    private MapGame mapEnemyView;

    private int[][] rangeShip = {{0, 1, 2, 3, 4, 5}, {1, 2, 3, 4, 5, 0}, {2, 3, 4, 5, 0, 1},
            {3, 4, 5, 0, 1, 2}, {4, 5, 0, 1, 2, 3}, {5, 0, 1, 2, 3, 4}, {0, 5, 4, 3, 2, 1}};

    private static Ship[] shipsPlayer = {new Ship(Ship.BATTLESHIP), new Ship(Ship.CRUISER), new Ship(Ship.CRUISER),
            new Ship(Ship.DESTROYER), new Ship(Ship.DESTROYER), new Ship(Ship.DESTROYER),
            new Ship(Ship.BOAT), new Ship(Ship.BOAT), new Ship(Ship.BOAT), new Ship(Ship.BOAT)};

    private static Ship[] shipsEnemy = {new Ship(Ship.BATTLESHIP), new Ship(Ship.CRUISER), new Ship(Ship.CRUISER),
            new Ship(Ship.DESTROYER), new Ship(Ship.DESTROYER), new Ship(Ship.DESTROYER),
            new Ship(Ship.BOAT), new Ship(Ship.BOAT), new Ship(Ship.BOAT), new Ship(Ship.BOAT)};

    private int countCellShips = 20;
    private Point startCreatePoint;

    private boolean isGame = true;


    public BattleSea() {

        mapPlayer = new MapGame(MapGame.MAP_PLAYER);
        mapEnemyShip = new MapGame(MapGame.MAP_ENEMY);
        mapEnemyView = new MapGame(MapGame.MAP_ENEMY);

        generateShipMap(mapPlayer, shipsPlayer);
        generateShipMap(mapEnemyShip, shipsEnemy);
        generateShipMap(mapEnemyView, shipsEnemy);
    }

    public void start(MapGame map, boolean isCPU, String nameMap) {

        map.render();
        char alphabet;
        int number;

        if (isCPU) {
            alphabet = PointShip.KEY_ALPHABET[new Random().nextInt(9)];// move cpu
            number = new Random().nextInt(9);
            move(map, shipsPlayer, alphabet, number, nameMap);
            map.render();
            isCPU = false;
        } else {
            Scanner scanner = new Scanner(System.in);
            String symbol = scanner.nextLine();
            alphabet = symbol.charAt(0);// move player
            number = Integer.parseInt(symbol.substring(1));
            number--;
            move(map, shipsEnemy, alphabet, number, nameMap);
            map.render();
            isCPU = true;
        }


        if (countCellShips == 0) {
            isGame = false;
            System.out.println("You're Win");
        }
    }


    private void move(MapGame map, Ship[] ships, char alphabet, int number, String nameMap) {
        PointShip point = new PointShip(alphabet, number);

        switch (map.getCell(point.getX(), point.getY())) {
            case MapGame.SHIP:
                map.setMap(point.getX(), point.getY(), MapGame.WOUNDED);
                System.out.println("WOUNDED");
                int nShip = searchWoundedShip(ships, point.getX(), point.getY());

                if (nameMap.equals(MapGame.MAP_PLAYER)) {
                    mapEnemyView.setMap(point.getX(),point.getY(),MapGame.WOUNDED);
                }

                if (ships[nShip].getCell().isEmpty()) {
                    for (int i = 0; i < ships[nShip].getDeathCell().size(); i++) {
                        map.setMap((int) ships[nShip].getDeathCell().get(i).getX(), (int) ships[nShip].getDeathCell().get(i).getY(),MapGame.KILLED);
                        System.out.println("KILLED");
                        if (nameMap.equals(MapGame.MAP_PLAYER)) {
                            mapEnemyView.setMap(point.getX(), point.getY(),MapGame.KILLED);
                        }
                    }
                }
                countCellShips--;
                break;
            case MapGame.WAVE:
                map.setMap(point.getX(),point.getY(),MapGame.MISSED);
                System.out.println("MISSED");
                break;
        }
    }

    private int searchWoundedShip(Ship[] ships, int x, int y) {
        int temp;
        int numberShipArray = 0;
        for (int i = 0; i < ships.length; i++) {
            if ((temp = ships[i].getCell().indexOf(new Point(x, y))) != -1) {
                ships[i].removeCell(temp);//
                numberShipArray = i;
                break;
            }
        }
        return numberShipArray;
    }


    private void generateShipMap(MapGame map, Ship[] ships) {
        boolean temp = true;

        for (int i = 0; i < ships.length; i++) {
            while (temp) {
                int x = new Random().nextInt(10 - ships[i].getLength());
                int y = new Random().nextInt(10 - ships[i].getLength());
                boolean latLong = new Random().nextBoolean();
                if (checkMapShip(map, x, y, ships[i].getLength(), latLong)) {
                    writeShipMap(map, ships, x, y, latLong, i);
                    temp = false;
                }
            }
            temp = true;
        }
    }

    private void writeShipMap(MapGame map, Ship[] ships, int x, int y, boolean latitudeOrLongitude, int numberShip) {
        for (int i = 0; i < ships[numberShip].getLength(); i++) {
            if (latitudeOrLongitude) {
                map.setMap(x + i, y, MapGame.SHIP);
                ships[numberShip].setCell(new Point(x + i, y));
            } else {
                map.setMap(x, i + y, MapGame.SHIP);
                ships[numberShip].setCell(new Point(x, i + y));
            }
        }
    }


    private static boolean checkMapShip(MapGame map, int x, int y, int length, boolean latitudeOrLongitude) {
        boolean isCreate = true;
        for (int i = 0; i < length; i++) {
            if (latitudeOrLongitude) {
                if (!map.getMap()[x + i][y].equals(MapGame.WAVE)) {
                    isCreate = false;
                    break;
                }
            } else {
                if (!map.getMap()[x][i + y].equals(MapGame.WAVE)) {
                    isCreate = false;
                    break;
                }
            }
        }
        return isCreate;
    }

    public boolean isGame() {
        return isGame;
    }
    public MapGame getMapPlayer() {
        return mapPlayer;
    }

    public MapGame getMapEnemyShip() {
        return mapEnemyShip;
    }

    public MapGame getMapEnemyView() {
        return mapEnemyView;
    }
/*
    public Ship[] getShips() {
        return ships;
    }


    public static boolean testHard(String[][] map) {
        int count = 0;
        boolean shipIsWrite = false;
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (!map[i][j].equals(WAVE)) {
                    count++;
                    if (count == 20) {
                        shipIsWrite = true;
                    }
                }
            }
        }
        return shipIsWrite;
    }

    public void hard(String[][] map) {
        int randomArray = new Random().nextInt(rangeShip.length);

        for (int i = 0; i < 6; i++) {
            createShipHard(ships[rangeShip[randomArray][i]], map);
        }
    }

    public void createShipHard(Ship ship, String[][] map) {
        if (startCreatePoint == null) {
            int y = new Random().nextInt(3);
            int x = 0;
            writeShipMap(map, x, y, false, ship.getLength());
            startCreatePoint.setLocation(x, (y + ship.getLength() + 1));
        } else if ((startCreatePoint.y + ship.getLength() + 1) > 9) {
            writeShipMap(map, startCreatePoint.x, startCreatePoint.y, false, ship.getLength());
            startCreatePoint.setLocation(startCreatePoint.x, (startCreatePoint.y + ship.getLength() + 1));
        } else if ((startCreatePoint.y + ship.getLength() + 1) > 9) {
            writeShipMap(map, 9, startCreatePoint.y, false, ship.getLength());
            startCreatePoint.setLocation(9, (startCreatePoint.y + ship.getLength() + 1));
        }
    }

    public int randomCell() {
        int randomInt = new Random().nextInt(4);
        return randomInt;
    }
    */
}