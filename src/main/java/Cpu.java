import java.util.ArrayList;
import java.util.Random;

public class Cpu {


    private ArrayList<PointShip> lastWounded = new ArrayList<>();// for point wounded;
    private ArrayList<PointShip> secondWounded = new ArrayList<>();
    private int countCenter = 0;
    private int countSouth = 0;
    private int countNorth = 0;
    private int countWest = 0;
    private int countEast = 0;

    private boolean CENTER;
    private boolean SOUTH;
    private boolean NORTH;
    private boolean WEST;
    private boolean EAST;
    private boolean SOUTH_WEST;
    private boolean SOUTH_EAST;
    private boolean NORTH_WEST;
    private boolean NORTH_EAST;


    public PointShip getWoundedCell(int x, int y) {
        ArrayList<PointShip> maybeWounded = new ArrayList<>();

        if (lastWounded.size() == 1) {
            if ((x >= 2 && x <= 8) && (y >= 2 && y <= 8)) {
                maybeWounded.add(new PointShip(x--, y));
                maybeWounded.add(new PointShip(x, y--));
                maybeWounded.add(new PointShip(x++, y));
                maybeWounded.add(new PointShip(x, y++));
                this.countCenter++;
                return maybeWounded.get(new Random().nextInt(maybeWounded.size()));
            }

            if (x >= 2 && y == 9 && x <= 8) {
                maybeWounded.add(new PointShip(x--, y));
                maybeWounded.add(new PointShip(x, y--));
                maybeWounded.add(new PointShip(x++, y));
                this.countSouth++;
                return maybeWounded.get(new Random().nextInt(maybeWounded.size()));
            }

            if (x >= 2 && y == 1 && x <= 8) {
                maybeWounded.add(new PointShip(x--, y));
                maybeWounded.add(new PointShip(x, y++));
                maybeWounded.add(new PointShip(x++, y));
                this.countNorth++;
                return maybeWounded.get(new Random().nextInt(maybeWounded.size()));
            }

            if (y >= 2 && y <= 8 && x == 1) {
                maybeWounded.add(new PointShip(x, y--));
                maybeWounded.add(new PointShip(x++, y));
                maybeWounded.add(new PointShip(x, y++));
                this.countWest++;
                return maybeWounded.get(new Random().nextInt(maybeWounded.size()));
            }

            if (y >= 2 && y <= 8 && x == 8) {
                maybeWounded.add(new PointShip(x, y--));
                maybeWounded.add(new PointShip(x--, y));
                maybeWounded.add(new PointShip(x, y++));
                this.countEast++;
                return maybeWounded.get(new Random().nextInt(maybeWounded.size()));
            }
        }

        if (lastWounded.size() == 2) {
            if ((lastWounded.get(0).x == lastWounded.get(1).x)) {
                if (lastWounded.get(1).x == 1) {
                    maybeWounded.add(new PointShip(x, y++));
                    return maybeWounded.get(0);
                }
                if (lastWounded.get(1).x == 8) {
                    maybeWounded.add(new PointShip(x, y--));
                    return maybeWounded.get(0);
                }
                if (lastWounded.get(1).x > 1 && lastWounded.get(1).x < 8) {
                    if (new Random().nextBoolean()) {
                        maybeWounded.add(new PointShip(x, y++));
                    } else {
                        maybeWounded.add(new PointShip(x, y--));
                    }
                    return maybeWounded.get(0);
                }
                //><
            } else if (lastWounded.get(0).y == lastWounded.get(1).y) {
                if (lastWounded.get(1).y == 1) {
                    maybeWounded.add(new PointShip(x++, y));
                    return maybeWounded.get(0);
                }
                if (lastWounded.get(1).y == 8) {
                    maybeWounded.add(new PointShip(x--, y));
                    return maybeWounded.get(0);
                }
                if (lastWounded.get(1).y > 1 && lastWounded.get(1).y < 8) {
                    if (new Random().nextBoolean()) {
                        maybeWounded.add(new PointShip(x++, y));
                    } else {
                        maybeWounded.add(new PointShip(x--, y));
                    }
                    return maybeWounded.get(0);
                }
                //><
            }
        }

        if (lastWounded.size() == 3) {
            int number = 0;
            if (lastWounded.get(0).x == lastWounded.get(1).x) {
                int minMax[] = new int[2];
                for (int i = 0; i < lastWounded.size(); i++) {
                    if (minMax[0] < lastWounded.get(i).y) {
                        minMax[0] = lastWounded.get(i).y;
                    }
                    if(minMax[1] > lastWounded.get(i).y){
                        minMax[1] = lastWounded.get(i).y;
                    }
                }
                number = minMax[new Random().nextInt(minMax.length)];
            }
            
            return new PointShip(lastWounded.get(0).x,number);
        }
        return null;
    }

    public PointShip getRandomHit() {
        return new PointShip(new Random().nextInt(9), new Random().nextInt(9));
    }

    public ArrayList<PointShip> getLastWounded() {
        return lastWounded;
    }

    public void setLastWounded(ArrayList<PointShip> lastWounded) {
        this.lastWounded = lastWounded;
    }
}